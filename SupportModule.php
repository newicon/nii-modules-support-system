<?php

/**
 * SupportModule class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Support Module class
 */
class SupportModule extends NWebModule
{
	/**
	 * Module name
	 * @var string 
	 */
	public $name = 'Support';
	
	public function init()
	{
		Yii::import('support.models.*');
		Yii::import('support.components.*');
	}
	
	/**
	 * setup the support module
	 */
	public function setup()
	{
		Yii::app()->menus->addItem('main', 'Support', TicketRoute::getRouteDashboard(), null);
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/tickets.css');
	}
	
	/**
	 * 
	 * @param type $controller
	 * @param type $action
	 * @return type
	 */
	public function beforeControllerAction($controller, $action)
	{
		$active = NHtml::isUrl('support');
		Yii::app()->menus->activateItem('main', 'Support');
		return parent::beforeControllerAction($controller, $action);
	}
	
	/**
	 * install for tickets module
	 */
	public function install()
	{
		NActiveRecord::install('SupportTicket');
		NActiveRecord::install('SupportEmail');
		NActiveRecord::install('SupportResponse');
	}
	
}