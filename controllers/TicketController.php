<?php

/**
 * TicketsController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Controller responsible for managing single tickets
 */
class TicketController extends AController
{
	/**
	 * Ticket layout
	 * @var string
	 */
	public $layout = '/layout/page';
	
	/**
	 * Display a single ticket
	 * @param int $id ticket id
	 */
	public function actionIndex($id)
	{
		$ticket = NData::loadModel('SupportTicket', $id, "No ticket found with id '$id'");
		
		$this->pageTitle = "[#$id] {$ticket->subject}";
		
		$this->render('index', array(
			'ticket'=>$ticket, 
			'contact'=>$ticket->contact()));
	}
	
	/**
	 * Add a new ticket
	 */
	public function actionAdd()
	{
		$model = new SupportTicket;
		$this->performAjaxValidation($model, 'ticket');
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_POST['SupportTicket'];
			if ($model->save()) {
				// successful save
				if (isset($_POST['save'])) 
					$this->redirect($model->route);
				if (isset($_POST['saveNew'])) {
					$this->redirect(TicketRoute::getRouteTicketAdd());
				}
				if (isset($_POST['saveClose']))
					$this->redirect(TicketRoute::getRouteTicketList());
			}
		}
		$this->render('add', array('model'=>$model));
	}
	
	/**
	 * Edit a ticket item
	 * @param int $id specify the id get parameter to be the ticket id
	 */
	public function actionEdit($id)
	{
		$model = NData::loadModel('SupportTicket', $id, "No ticket found with id '$id'");
		$this->performAjaxValidation($model, 'ticket');
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_POST['SupportTicket'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully updated ticket.');
				$this->redirect($model->route);
			}
		}
		$this->render('edit', array('model'=>$model));
	}
	
	/**
	 * Soft delete a ticket item (trash it)
	 * @param int $id the ticket id
	 */
	public function actionDelete($id)
	{
		$ticket = NData::loadModel('SupportTicket', $id, "No ticket found with id '$id'");
		if ($ticket->trash()) {
			Yii::app()->user->setFlash('success', 'Successfully removed ticket. <a class="btn" href="'.NHtml::url(TicketRoute::getRouteTicketRestore($id)).'"><i class="icon-share-alt"></i> Undo</a>');
			$this->redirect(TicketRoute::getRouteTicketList());
		} else {
			Yii::app()->user->setFlash('error', 'Failed to removed ticket.');
			$this->redirect(TicketRoute::getRouteTicketItem($id));
		}
	}
	
	/**
	 * restore a ticket that has been removed "trashed"
	 * @see self::actionDelete
	 */
	public function actionRestore($id)
	{
		$t = SupportTicket::model()->withRemoved()->findByPk($id);
		if ($t === null)
			throw new CHttpException(404, "No ticket found with id '$id'");
		$t->restore();
		$t->save();
		Yii::app()->user->setFlash('success', 'Successfully restored ticket.');
		$this->redirect($t->route);
	}
	
	/**
	 * raw html email output to view from iframe or popup
	 */
	public function actionIframe($ticketId)
	{
		$this->prepareAjaxResponse();
		$this->layout = 'ajax';
		$ticket = SupportTicket::model()->findByPk($ticketId);
		$this->render('iframe', array('ticket'=>$ticket));
	}
}