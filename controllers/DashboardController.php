<?php

/**
 * DashboardController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of DashboardController
 *
 * @author 
 */
class DashboardController extends AController
{
	/**
	 * Ticket layout
	 * @var string
	 */
	public $layout = '/layout/page';
	
	/**
	 * Display the ticket dashboard page
	 */
	public function actionIndex()
	{
		$this->render('index');
	}
}