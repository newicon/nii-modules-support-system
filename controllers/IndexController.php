<?php

/**
 * IndexController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Controller is responsible for listing and filtering tickets
 */
class IndexController extends AController
{
	/**
	 * Ticket layout
	 * @var string
	 */
	public $layout = '/layout/page';
	
	/**
	 * list tickets
	 */
	public function actionIndex()
	{
//		$m = new Mongo("mongodb://hub.newicon:27017",array("connect" => TRUE)); // connect
//		$m->selectDB('support');
//		$col = $m->selectCollection($db, 'emails');
//		dp($col->findOne());
		$this->render('index');
	}
}