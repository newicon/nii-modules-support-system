<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of InboundController
 *
 * @author steve
 */
class InboundController extends NController 
{
	/**
	 * Postmark inbound support email hook - processer
	 */
	public function actionEmail()
	{
		try {
			$emailJson = file_get_contents('php://input');
			$email = json_decode($emailJson, true);

			if ($email != null) {
				// insert into mongo (for fun)
				$m = new MongoClient("mongodb://localhost:27017",array("connect" => TRUE)); // connect
				$db = $m->selectDB("support");
				$collection = $db->selectCollection("emails");
				$collection->insert($email);

				// save raw into db (why not?)
				$se = new SupportEmail;
				$se->email = $emailJson;
				$se->save();

				//check if we have a mailbox hash
				$hash = $email['MailboxHash'];
				if ($hash) {
					
					// the hash is an existing ticket id... lets find it
					$ticket = SupportTicket::model()->findByPk($hash);
					if ($ticket === null) {
						// no existing ticket found...
						// create a new one perhaps?
						$response = new SupportResponse;
						$response->ticket_id = $ticket->id;
						$response->email_id  = $se->id;
					}
						
					
				} else {
				
					// create a new ticket
					// subject, description, type_id, status_id, priority_id, requester_id
					$ticket = new SupportTicket;
					$ticket->subject = $email['Subject'] ? $email['Subject'] : 'No Subject';
					$ticket->description = html_entity_decode(utf8_encode($email['HtmlBody']));
					$ticket->type_id = 2; // default to incident
					$ticket->status_id = SupportTicket::STATUS_OPEN;
					$ticket->priority_id = SupportTicket::PRIORITY_MEDIUM;

					$emailRecord = CrmEmail::model()->findByAttributes(array('email'=>$email['FromFull']['Email']));

					if ($emailRecord === null) {
						// got to create a new one eek
						// first we need a contact
						$contact = new CrmContact;
						$contact->name = $email['FromFull']['Name'];
						$contact->save();
						$emailRecord = $contact->addEmail($email['FromFull']['Email']);
					} else {
						// get contact
						$contact = CrmContact::model()->findByPk($emailRecord->contact_id);
					}
					$ticket->requester_id = $contact->id;
					$ticket->via = 'email';
					$ticket->support_email_id = $se->id;
					$ticket->save();
				}
			}
		} catch(Exception $e){
			mail('steve@newicon.net', 'error!', $e->getMessage());
		}
	}
}
