<?php

/**
 * SupportHtml class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Support helper class
 */
class SupportHtml
{
	/**
	 * Get html to display a previous and next button in relation to the current ticket record
	 * @param SupportTicket $ticket record
	 * @return string Html
	 */
	public static function getPreviousNextButtons($ticket)
	{
		$previous = $ticket->getPreviousRecord();
		$next = $ticket->getNextRecord();
		
		$urlPrevious = '';
		if ($previous !== null)
			$urlPrevious = NHtml::url(TicketRoute::getRouteTicketItem($previous->id));
	
		$urlNext = '';
		if ($next !== null)
			$urlNext = NHtml::url(TicketRoute::getRouteTicketItem($next->id));
		
		$ret = '<div class="btn-group">';
		$ret .= '<a href="'.$urlPrevious.'" class="btn btn-small '.($previous ? '' : 'disabled').'" ><i class="icon-chevron-left"></i></a>';
		$ret .= '<a href="'.$urlNext.'" class="btn btn-small '.($next ? '' : 'disabled').'"><i class="icon-chevron-right"></i></a>';
		return $ret .'</div>';
	}
	
	/**
	 * Display the status badge
	 * @param SupportTicket $ticket
	 */
	public static function status($ticket)
	{
		$status = $ticket->getStatus();
		$statusClass = strtolower($status);
		return "<span class=\"badge badge-$statusClass\">$status</span>";
	}
}