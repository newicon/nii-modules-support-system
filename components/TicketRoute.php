<?php

/**
 * TicketRoute class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Class is responsible for defining ticket system routes
 */
class TicketRoute
{
	/**
	 * Get the route to an individual ticket
	 * @param int $ticketId the id of the ticket model item row
	 * @return array
	 */
	public static function getRouteTicketItem($ticketId)
	{
		return array('/support/ticket/index', 'id'=>$ticketId);
	}
	
	/**
	 * Get the route to the ticket index list page
	 * @return array
	 */
	public static function getRouteTicketList()
	{
		return array('/support/index/index');
	}
	
	/**
	 * Get the route to the ticket index list page
	 * @return array
	 */
	public static function getRouteTicketAdd()
	{
		return array('/support/ticket/add');
	}
	
	/**
	 * Get the route to edit ticket action
	 * @return array
	 */
	public static function getRouteTicketEdit($ticketId)
	{
		return array('/support/ticket/edit', 'id'=>$ticketId);
	}
	
	/**
	 * Get the route to the restore action (restores a trashed item)
	 * @param int $ticketId
	 */
	public static function getRouteTicketRestore($ticketId)
	{
		return array('/support/ticket/restore', 'id'=>$ticketId);
	}
	
	/**
	 * Get the route to delete this ticket (soft delete it trashes the item)
	 * @param int $ticketId
	 */
	public static function getRouteTicketDelete($ticketId)
	{
		return array('/support/ticket/delete', 'id'=>$ticketId);
	}
	
	/**
	 * Get the route to the dashboard page
	 * @return array
	 */
	public static function getRouteDashboard()
	{
		return array('/support/dashboard/index');
	}
	
	/**
	 * Get route to the ticket admin settings area
	 * @return array
	 */
	public static function getRouteAdmin()
	{
		return array('/support/admin/index');
	}
	
	/**
	 * Get the url to an individual ticket
	 * @param int $ticketId the id of the ticket model item row
	 * @return string url
	 */
	public static function getUrlTicketItem($ticketId)
	{
		return NHtml::url(self::getRouteTicketItem($ticketId));
	}
	
	/**
	 * Get the url to the ticket index list page
	 * @return string url
	 */
	public static function getUrlTicketList()
	{
		return NHtml::url(self::getRouteTicketList());
	}
	
	/**
	 * get the url to add a new ticket
	 * @return string url
	 */
	public static function getUrlTicketAdd()
	{
		return NHtml::url(self::getRouteTicketAdd());
	}
	
	/**
	 * Get the url to edit a ticket
	 * @param int $ticketId
	 * @return string url
	 */
	public static function getUrlTicketEdit($ticketId)
	{
		return NHtml::url(self::getRouteTicketEdit($ticketId));
	}
	
	/**
	 * Get the url to the restore action (restores a trashed item)
	 * @param int $ticketId
	 */
	public static function getUrlTicketRestore($ticketId)
	{
		return NHtml::url(self::getRouteTicketRestore($ticketId));
	}
	
	/**
	 * Get the url to delete this ticket (soft delete it trashes the item)
	 * @param int $ticketId
	 */
	public static function getUrlTicketDelete($ticketId)
	{
		return NHtml::url(self::getRouteTicketDelete($ticketId));
	}
	
	/**
	 * get the url to the dashboard page
	 * @return string url
	 */
	public static function getUrlDashboard()
	{
		return NHtml::url(self::getRouteDashboard());
	}
	
	/**
	 * Get the string url to the admin section
	 * @return url
	 */
	public static function getUrlAdmin()
	{
		return NHtml::url(self::getRouteAdmin());
	}
}