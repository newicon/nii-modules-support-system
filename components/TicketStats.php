<?php

/**
 * TicketStats class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Helper function to define count and stat queries.
 */
class TicketStats
{
	public static function getCountOpenAndUnassigned()
	{
		return SupportTicket::model()->countByAttributes(array('agent_id'=>0, 'status_id'=>SupportTicket::STATUS_OPEN));
	}
	
	public static function getCountOpen()
	{
		return SupportTicket::model()->open()->count();
	}
	
	public static function getCountPending()
	{
		return SupportTicket::model()->countByAttributes(array('status_id'=>SupportTicket::STATUS_PENDING));
	}
	
	public static function getCountOverdue()
	{
		return 0;
	}
}
