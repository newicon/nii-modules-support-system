<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div>
	<a href="<?php echo TicketRoute::getUrlTicketAdd(); ?>" class="btn btn-primary">Add Ticket</a>
</div>
<br />
<ul class="stats nav">
	<li>
		<a href="<?php echo TicketRoute::getUrlTicketList(); ?>" class="stat">
			<span class="stat-name mbl">Overdue</span>
			<span class="stat-value"><?php echo TicketStats::getCountOverdue(); ?></span>			
		</a>
	</li>
	<li>
		<a href="<?php echo TicketRoute::getUrlTicketList(); ?>" class="stat">
			<span class="stat-name mbl">Open</span>
			<span class="stat-value"><?php echo TicketStats::getCountOpen(); ?></span>			
		</a>
	</li>
	<li>
		<a href="<?php echo TicketRoute::getUrlTicketList(); ?>" class="stat">
			<span class="stat-name mbl">Pending</span>
			<span class="stat-value"><?php echo TicketStats::getCountPending(); ?></span>			
		</a>
	</li>
	<li>
		<a href="<?php echo TicketRoute::getUrlTicketList(); ?>" class="stat">
			<span class="stat-name mbl">Due Today</span>
			<span class="stat-value">-</span>			
		</a>
	</li>
	<li>
		<a href="<?php echo TicketRoute::getUrlTicketList(); ?>" class="stat" >
			<span class="stat-name mbl" rel="tooltip" title="Tickets that are open and not assigned">Unassigned</span>
			<span class="stat-value"><?php echo TicketStats::getCountOpenAndUnassigned(); ?></span>			
		</a>
	</li>
</ul>