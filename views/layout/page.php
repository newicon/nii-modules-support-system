<?php $this->beginContent('//layouts/admin1column'); ?>

<style>
.nav-tabs > li {
	padding: 5px 0px 0px 0px;
	font-size: 13px;
}
.nav-tabs > li > a {
	padding-top: 5px;
	padding-bottom: 5px;
}
/*.ticket-nav{margin:-10px -20px 10px -20px;padding: 0px 15px 0px 15px;background-color:#f1f1f1;}*/
</style>
<ul class="ticket-nav nav nav-tabs">
	<li class="<?php echo NHtml::active('/tickets/dashboard') ?>">
		<a href="<?php echo TicketRoute::getUrlDashboard() ?>">Dashboard</a>
	</li>
	<li class="<?php echo NHtml::active('/tickets/index'); echo NHtml::active('/tickets/ticket') ?>">
		<a href="<?php echo TicketRoute::getUrlTicketList() ?>">Tickets</a>
	</li>
	<li class="<?php echo NHtml::active('/tickets/admin'); ?>">
		<a href="<?php echo TicketRoute::getUrlAdmin() ?>">Admin</a>
	</li>
	<a class="pull-right btn btn-primary btn-small pull-right mts" href="<?php echo TicketRoute::getUrlTicketAdd() ?>"><i class="icon-plus-sign icon-white"></i> Add New Ticket</a>
</ul>

<?php echo $content ?>

<?php $this->endContent(); ?>