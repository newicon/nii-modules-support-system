<?php
/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<style>
	.ticket-grid .col-check{width:25px;}
	.badge-ticket-id{background-color:#ccc;}
</style>
Tickets



<div class="line">

	<div class="unit" style="width:200px;">
		Filters
	</div>

	<div class="lastUnit">
		<table class="table ticket-grid">
			<thead>
				<tr>
					<th>d</th>
					<th>d</th>
					<th>d</th>
				</tr>
			</thead>
			<?php foreach (SupportTicket::model()->findAll() as $ticket) : ?>
				<?php $contact = $ticket->contact; ?>
				<tr>
					<td class="col-check"><input type="checkbox"/></td>
					<td>
						<div class="media">
							<a class="pull-left" href="#">
								<img class="media-object" src="http://placehold.it/42x42">
							</a>
							<div class="media-body">
								<span class="badge badge-ticket-id"><?php echo $ticket->id; ?></span>
								<a href="<?php echo NHtml::url($ticket->route) ?>"><?php echo $ticket->subject ?></a>
								<br/>
								<small><span class="muted">From:</span> <?php echo $contact ? $contact->name : '' ?> , <span class="muted">Created:</span> <?php echo NTime::timeAgo($ticket->created_at) ?></small>
							</div>
						</div>
					</td>
					<td>
						Agent: <?php echo $ticket->getAgent(); ?><br/>
						Status: <?php echo SupportHtml::status($ticket); ?> <br/>
						Priority: <?php echo $ticket->priority ?><br/>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
</div>