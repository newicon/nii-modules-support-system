<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<ul class="nav nav-pills">
	<li class="<?php echo NHtml::active('/tickets/dashboard') ?>">
		<a href="<?php echo TicketRoute::getUrlDashboard() ?>">Dashboard</a>
	</li>
	<li class="<?php echo NHtml::active('/tickets/index'); echo NHtml::active('/tickets/ticket') ?>">
		<a href="<?php echo TicketRoute::getUrlTicketList() ?>">Tickets</a>
	</li>
	<li class="<?php echo NHtml::active('/tickets/admin'); ?>">
		<a href="<?php echo TicketRoute::getUrlTicketList() ?>">Admin</a>
	</li>
</ul>