<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<style>
	.ticket-convo.media{border-left:4px solid #ddd;overflow:visible;margin:0 0 0 37px;padding:0px 0px 20px 30px;position:relative;}
	.ticket-convo .media-image{position:absolute;left:-30px;}
	.ticket-convo .media-image img{border-radius: 30px;padding: 3px;border: 1px solid #ddd;background:white;}
	.ticket-convo .speech-bubble:before, .ticket-convo .speech-bubble:after  {top:32px;}
	
	.image-bubble img{border-radius: 30px;padding: 3px;border: 1px solid #ddd;background:white;}
	
	.ticket-convo .speech-bubble{border-color:#ddd;box-shadow: none;}
	.speech-bubble:before{border-right-color:#ddd}
	.ticket-convo:hover .speech-bubble{border-color:#abb3d1;box-shadow: 0px 1px 2px rgba(0,0,0,0.3);}
	.ticket-convo:hover .speech-bubble:before {border-right-color:#abb3d1}
	.ticket-convo:hover .image-bubble img{border-color:#abb3d1;}
	
	.ticket-title{font-weight:normal;font-size:18px;margin-top:0px;}
	
	/** Support stub is the subject and stub of the ticket, it stays fixed. */
	.ticket-stub{}
 
	/*
     * Support root is the root message
	 */
	.ticket-root.media {padding-top:20px;}
	.ticket-root .speech-bubble{background-color:#fefef0;box-shadow:0 4px 6px rgba(130,130,130,0.1);margin-bottom:6px;}
	.ticket-root.ticket-convo:hover .speech-bubble{box-shadow: 0px 4px 6px rgba(0,0,0,0.1);}
	
	.ticket-actions	.input-box{padding:10px;border:1px solid #ccc;}
	.ticket-actions.media{border-left:none;margin-left:41px;}
</style>

<div class="well well-transparent">

	<div id="uploader">UPLOADER!!</div>
	
	
<div id="ticket-fixed">
	<div class="row-fluid pbm">
		<div class="span2 pts">
			<a href="<?php echo TicketRoute::getUrlTicketList(); ?>">Tickets</a> <span class="muted">&rtrif;</span> <strong>Ticket #<?php echo $ticket->id; ?></strong>
		</div>
		<div class="span6 text-right">
			<span class="label label-warning" style="padding:6px 12px">OPEN</span>
			<div class="btn-group">
				<a href="" class="btn disabled btn-small"><i class="icon-share-alt"></i> Reply</a>
				<a href="" class="btn disabled btn-small"><i class="icon-arrow-right"></i> Forward</a>
			</div>
			<a href="<?php echo TicketRoute::getUrlTicketEdit($ticket->id); ?>" class="btn btn-small"><i class="icon-pencil"></i> Edit</a>
		</div>
		<div class="span4 text-right">
			<?php echo SupportHtml::getPreviousNextButtons($ticket) ?>
		</div>
	</div>
	<div class="ticket-stub mtn">
		<!--<h2 class="ticket-title"><?php echo $ticket->subject; ?></h2>-->
		
		<!--<div class="alert">OPEN</div>-->
	</div>
	<!--<hr>-->
</div>
	
<div class="row-fluid">
	<div class="span8">
		<div class="media ticket-convo ticket-root ptn">
			<div class="media-image image-bubble">
				<img width="48" src="<?php echo $contact->image_url; ?>">
			</div>
			<div class="media-body">
				<div class="speech-bubble ">
					<h4 class="mtn mbs"><?php echo $ticket->subject; ?></h4>
					<strong><a href="<?php echo $contact->url; ?>"><?php echo $contact->name; ?></a></strong> 
					<small class="muted" style="margin-left:5px;"><span class="activity-date">May 22nd</span></small> <br>
					
					
					<ul id="myTab" class="nav nav-tabs">
						<li class="active"><a href="#html" data-toggle="tab">Html</a></li>
						<li><a href="#text" data-toggle="tab">Text</a></li>
					</ul>
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane fade in active" id="html">
							<iframe id="iframeEmailViewer" style="width:100%;margin-top:5px;" src="<?php echo NHtml::url(array('/support/ticket/iframe', 'ticketId'=>$ticket->id)) ?>" seamless onload="iframeLoaded()"></iframe>
						</div>
						<div class="tab-pane fade" id="text">
							<?php echo nl2br($ticket->getTextBody()) ?>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<?php foreach($ticket->responses as $response): ?>
			<div class="media ticket-convo">
				<div class="media-image image-bubble">
					<img src="http://www.gravatar.com/avatar/84a6778f82775b54a5f7ba38c916f68f?s=48&amp;r=G&amp;d=mm">
				</div>
				<div class="media-body">
					<div class="speech-bubble ">
						<strong><a href="/hub.newicon.net/htdocs/user/profile/index/id/3">Steve O'Brien</a></strong> 
						<small class="muted" style="margin-left:5px;"><span class="activity-date"><?php echo NTime::timeAgo($response->updated_at) ?></span></small> <br>
						<?php //echo $response->message; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
		
		<!-- Support tools -->
		<div class="media ticket-convo ticket-actions">
			<div class="media-image image-bubble">
				<img src="http://www.gravatar.com/avatar/84a6778f82775b54a5f7ba38c916f68f?s=48&amp;r=G&amp;d=mm">
			</div>
			<div class="media-body">
				<div class="speech-bubble" style="background-color:#eee;">
<!--				<div class="input-box">
						<a class="btn-link" onclick="$('.input-box').hide();$('#ticket-reply').show();">Reply</a> / <a class="btn-link">Forward</a> / <a class="btn-link">Add Note</a>
					</div>-->
					
					<div id="ticket-forms">
						<div id="ticket-reply">
							<textarea id="ticket-reply-text" style="width:100%;height:100%;"></textarea>
							<button id="ticket-send" class="btn">Send</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span4 ptm">
		<?php $this->renderPartial('_ticket-properties', array('ticket'=>$ticket)); ?>
	</div>
</div>
</div>
<script type="text/javascript">
  function iframeLoaded() {
      var iFrameID = document.getElementById('iframeEmailViewer');
      if(iFrameID) {
            // here you can make the height, I delete it first, then I make it again
            iFrameID.height = "";
            iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
      }   
  }
  
$(function(){
	
	$('#ticket-send').click(function(){
		var txt = $('#ticket-reply-text').val();
		 $.post('support/ticket/reply', {tid:<?php echo $ticket->id; ?>,reply:txt});
	});
	
//		var funs = {
//			filesAdded:function(up, files){
//				// remove multiple files so we only upload the last one
//				if(up.files.length > 1)
//					this.uploader.splice(0,up.files.length-1);
//				if(up.files.length > 0) {
//					this.uploader.start();
//					this.setModeLoading();
//				}
//			},
//			uploadProgress:function(up, file){},
//			fileUploaded:function(up, file, info){
//				var json = $.parseJSON(info.response);
//				this.model.set({id:json.id, url:json.url})
//				this.setImage(json.url);
//			},
//			uploadComplete:function(up, file, info){
//
//			},
//			error:function(up, err){
//				alert(err.message + (err.file ? ", File: " + err.file.name : ""));
//				up.refresh(); // Reposition Flash/Silverlight
//			},
//		};
//	
//		var uploader = new plupload.Uploader({
//			runtimes : "html5,flash",
//			browse_button : "pickfiles",
//			max_file_size : '10mb',
//			drop_element : "user-photo-picker-drop",
//			url : "<?php echo NHtml::url('nii/widget/plupload'); ?>",
//			flash_swf_url:"<?php echo $this->createWidget('nii.widgets.plupload.PluploadWidget')->getAssetsUrl(); ?>/plupload.flash.swf",
//			filters: [{
//				title: "Image files",
//				extensions: "jpg,jpeg,png,gif"
//			}]
//		});
//		uploader.init();
//		if(!uploader.features.dragdrop){
//			alert('Your browser does not support drag and drop uploading.')
//		}
//		uploader.bind('FilesAdded',	funs.filesAdded);
//		uploader.bind('UploadProgress',funs.uploadProgress);
//		uploader.bind('FileUploaded',	funs.fileUploaded);
//		uploader.bind('UploadComplete', funs.uploadComplete);
//		uploader.bind('Error',			funs.error);
			
	

});
</script>  