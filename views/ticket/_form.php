<?php 
/**
 * requires:
 * @params TicketTicket $model model
 */
?>

<style>
.input-box{
	background-color: #ffffff;
	border: 1px solid #cccccc;
	border-radius:4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	-webkit-transition: border linear 0.2s, box-shadow linear 0.2s;
	-moz-transition: border linear 0.2s, box-shadow linear 0.2s;
	-o-transition: border linear 0.2s, box-shadow linear 0.2s;
	transition: border linear 0.2s, box-shadow linear 0.2s;
	padding:0px 5px;
}
</style>

<div class="well">
	
<?php $form = $this->beginWidget('nii.widgets.NActiveForm', array(
	'id'=>'ticket',
	'htmlOptions'=>array('class'=>''),
	'action'=>  $model->isNewRecord ? TicketRoute::getUrlTicketAdd() : TicketRoute::getUrlTicketEdit($model->id),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	)
)); ?>
	
<?php echo $this->createWidget('nii.widgets.select2.Select2')->registerScripts(); ?>
	
	<?php // echo CHtml::listBox('cc', array(), array('someting', 'steve'), array('class'=>'chosen', 'multiple'=>'multiple')); ?>
	
	<div class="control-group">
		<?php echo $form->labelEx($model, 'requester_id') ?>
		<div class="controls">
			<?php 
				$this->widget('nii.widgets.select2.Select2', array(
					'model'=>$model,
					'attribute'=>'requester_id',
					'data'=>  SupportCrmApi::getEmails(),
					'htmlOptions'=>array(
						'prompt'=>'...',
						'style'=>'width:542px',
					)
				));
			?>
			<a class="btn btn-link" onclick="$('#cc').toggle();">Add Cc</a>
			<?php echo $form->error($model, 'requester_id') ?>
		</div>
		
	</div>
	
		
	<div class="control-group" id="cc" style="display:none;">
		<?php echo $form->labelEx($model, 'cc') ?>
		<div class="controls">
			<?php 
				$this->widget('nii.widgets.select2.TagInput', array(
					'model'=>$model,
					'attribute'=>'cc',
					'settings'=>array(
						'tags'=>array('someone@test.com','another@somewhere.com'),
					),
					'htmlOptions'=>array(
						'style'=>'width:542px',
					)
				));
			?>	
		</div>
	</div>
		
	<div class="control-group">
		<?php echo $form->labelEx($model, 'subject') ?>
		<div class="controls">
			<?php echo $form->textField($model, 'subject', array('class'=>'input-xxlarge')) ?>
			<?php echo $form->error($model, 'subject') ?>
		</div>
	</div>
		
	

	<div class="row">
		<div class="control-group span6">
			<?php echo $form->labelEx($model, 'type_id') ?>
			<div class="controls">
				<?php echo $form->dropDownList($model, 'type_id', $model->getTypes(), array('prompt'=>'...', 'class'=>'chosen')) ?>
				<?php echo $form->error($model, 'type_id') ?>
			</div>
		</div>

		<div class="control-group span6">
			<?php echo $form->labelEx($model, 'status_id') ?>
			<div class="controls">
				<?php echo $form->dropDownList($model, 'status_id', $model->getStatesListData(), array('class'=>'chosen')) ?>
				<?php echo $form->error($model, 'status_id') ?>
			</div>
		</div>
	</div>
	<div class="row">
		
		<div class="control-group span6">
			<?php echo $form->labelEx($model, 'priority_id') ?>
			<div class="controls">
				<?php echo $form->dropDownList($model, 'priority_id', $model->getPriorities(), array('class'=>'chosen')) ?>
				<?php echo $form->error($model, 'priority_id') ?>
			</div>
		</div>

		<div class="control-group span6">
			<?php echo $form->labelEx($model, 'agent_id') ?>
			<div class="controls">
				<!-- replace this with a user widget for selecting users. Possibly need a mechanism for defining which users should display -->
				<?php echo $form->dropDownList($model, 'agent_id', CHtml::listData(User::model()->findAll(), 'id', 'name'), array('prompt'=>'...', 'class'=>'chosen')) ?>
				<?php echo $form->error($model, 'agent_id') ?>
			</div>
		</div>
		
	</div>
	
	<div class="control-group <?php echo NHtml::errorClass($model, 'description'); ?>" >
		<?php echo $form->labelEx($model, 'description') ?>
		<?php 
			$this->widget('nii.widgets.wysiwyg.redactor.Editor', array(
				// you can either use it for model attribute
				'model' => $model,
				'attribute' => 'description',

				// or just for input field
				//'name' => 'my_input_name',
				'plugins'=>array(
					'fullscreen' => array(
						'js' => array('fullscreen.js',),
					),
				),
				// some options, see http://imperavi.com/redactor/docs/
				'options' => array(
					'minHeight' => 250,
					'imageUpload' => NHtml::url('/nii/widget/redactorUpload'),
					'fileUploadCallback' => 'function(obj, json) {
						console.log(json);
					}'
				),
			));
		?>
		<?php echo $form->error($model, 'description') ?>
	</div>
	
	<div class="control-group">
		<div class="control-group" id="cc" style="">
			<?php echo $form->labelEx($model, 'tags') ?>
			<div class="controls">
				<?php 
//					$this->widget('nii.widgets.select2.TagInput', array(
//						'model'=>$model,
//						'attribute'=>'tags',
//						'settings'=>array(
//							'tags'=>$model->getModelTags(),
//						),
//						'htmlOptions'=>array(
//							'style'=>'width:542px',
//						)
//					));
				?>	
			</div>
		</div>
	</div>

	

<?php if ($model->isNewRecord): ?>
	<?php echo CHtml::submitButton('Save', array('class'=>'btn btn-primary', 'name'=>'save')); ?>
	<?php echo CHtml::submitButton('Save and Add Another', array('class'=>'btn btn-primary', 'name'=>'saveNew')); ?>
	<?php echo CHtml::submitButton('Save and Close', array('class'=>'btn btn-primary', 'name'=>'saveClose')); ?>
	<a href="<?php echo TicketRoute::getUrlTicketList() ?>" class="btn">Cancel</a>
<?php else: ?>
	<?php echo CHtml::submitButton('Update', array('class'=>'btn btn-primary', 'name'=>'Update')); ?>
	<a href="<?php echo TicketRoute::getUrlTicketDelete($model->id) ?>" class="btn btn-danger">Delete</a>
	<a href="<?php echo TicketRoute::getUrlTicketItem($model->id) ?>" class="btn">Cancel</a>
<?php endif; ?>

<?php $this->endWidget(); ?>

</div>