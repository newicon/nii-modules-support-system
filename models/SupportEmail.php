<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * SupportEmail
 *
 * @author steve
 */
class SupportEmail extends NActiveRecord
{
	public function tableName()
	{
		return '{{support_email}}';
	}
	
	public static function model($classNAme=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function schema() 
	{
		return array(
			'columns'=>array(
				'id'=>'pk', 
				// se postmark docs for info on json schema stored
				'email'=>'longtext COMMENT \'Store json string of email content\'',
				'created_at'=>'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP'
			)
		);
	}
}
