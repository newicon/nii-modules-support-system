<?php

/**
 * CrmApi class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * This class provides an interface between the ticket module and the CRM system being used
 * Thus future updates to the contacts module, CRM module or potentially the ability to hook something else in
 * should be relitively painless.
 */
class SupportCrmApi
{
	/**
	 * Get a list of email addresses in the system
	 * contact id => email
	 */
	public static function getEmails()
	{
		// select crm_email.id, contact_id, `name`, email from crm_email inner join crm_contact on contact_id = crm_contact.id
		$emailQ = CrmEmail::model()->cmd()
			->select('crm_email.id, contact_id, email, name')
			->join('crm_contact', 'crm_contact.id = contact_id')
			->queryAll();
		// sometimes the current contact module returns empty emails
		// lets only return valid emails
		$emails = array();
		foreach ($emailQ as $email) {
			$validator = new CEmailValidator();
			if ($validator->validateValue($email['email'])) {
				$emails[$email['id']] = $email['name'] . ' <' . $email['email'] . '>';
			}
		}
		return $emails;
	}
}