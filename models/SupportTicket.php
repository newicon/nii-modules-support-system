<?php

/**
 * SupportTicket class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Ticket Model 
 * @property int $requester_id
 * @property string $from store the original from email address
 * @property string $cc
 * @property string $subject
 * @property string $description
 * @property int $type_id
 * @property int $priority_id
 * @property int $status_id 
 * @property int $agent_id user id assigned
 * @property string $via 
 */
class SupportTicket extends NActiveRecord
{
	const PRIORITY_LOW = 1;
	const PRIORITY_MEDIUM = 2;
	const PRIORITY_HIGH = 3;
	const PRIORITY_URGENT = 4;
	
	// it may be necessary for people to add and configure additional
	// states in the future. However these are the core system states,
	// that would always exist
	const STATUS_OPEN = 1;
	const STATUS_PENDING = 2;
	const STATUS_RESOLVED = 3;
	const STATUS_CLOSED = 4;
	const STATUS_WAITING_ON_CUSTOMER = 5;
	
	public function tableName()
	{
		return '{{support_ticket}}';
	}
	
	public function relations()
	{
		return array(
			'email'=>array(self::BELONGS_TO, 'SupportEmail', 'support_email_id'),
			'contact'=>array(self::BELONGS_TO, 'CrmContact', 'requester_id'),
		);
	}
	
	public function getContact()
	{
		return  CrmContact::model()->findByPk($this->requester_id);
	}
	
	public function getEmailArray()
	{
		$emailRecord = $this->email();
		return json_decode($emailRecord->email, true);
	}
	
	public function getHtmlBody()
	{
		$email = $this->getEmailArray();
		if (array_key_exists('HtmlBody', $email)) {
			return html_entity_decode(utf8_encode($email['HtmlBody']));
		}
		return false;
	}
	
	public function getTextBody()
	{
		$email = $this->getEmailArray();
		return $email['TextBody'];
	}
	
	/**
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('subject, description, type_id, status_id, priority_id, requester_id', 'required'),
			array('via, cc', 'safe'),
		);
	}
	
	/**
	 * Ticket table schema
	 * @return array
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				// email id (CrmEmail) of the contact person who raised the request
				'requester_id'=>'int',
				'support_email_id'=>"int COMMENT 'The foreign key of the support_email this ticket was raised from'",
				// store the original from email address
				'from'=>'string',
				'cc'=>'text',
				'subject'=>'text',
				'description'=>'text',
				'type_id'=>'int',
				'priority_id'=>'int',
				'status_id'=>'int',
				'agent_id'=>'int not null default 0',
				// string representing where the ticket was raised from
				// i.e. email, phone, web form etc
				'via'=>'string not null default \'system\'',
			)
		);
	}
	
	/**
	 * Apply behaviors
	 * @return array
	 */
	public function behaviors()
	{
		return array(
			'trash'=>array(
				'class'=>'nii.components.behaviors.NTrashBinBehavior',
			),
			'tag'=>array(
				'class'=>'nii.components.behaviors.NTaggable'
            ),
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
            )
		);
	}
	
	/**
	 * Get static model instance
	 * @param string $className
	 * @return SupportTicket
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Define default scopes
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'open'=>array(
				'condition'=>'status_id = ' . self::STATUS_OPEN,
			),
			'pending'=>array(
				'condition'=>'status_id = ' . self::STATUS_PENDING,
			),
			'pending'=>array(
				'condition'=>'status_id = ' . self::STATUS_RESOLVED,
			),
			'closed'=>array(
				'condition'=>'status_id = ' . self::STATUS_CLOSED,
			)
		);
	}
	
	/**
	 * column => label
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'type_id'=>'Type',
			'status_id'=>'Status',
			'priority_id'=>'Priority',
			'requester_id'=>'Requester'
		);
	}
	
	/**
	 * Function to return an associative array of types.
	 * id => type for now this is a function, later this could be put into a lookup table
	 * if the need to modify them occurs. Then this function would simply lookup the db
	 * @return array id=>type
	 */
	public function getTypes()
	{
		return array(
			1=>'Question',
			2=>'Incident',
			3=>'Problem',
			4=>'Feature Request',
			5=>'Lead'
		);
	}
	
	/**
	 * Return an associative array of priorities, id => priority string
	 * This function can later be converted to lookup priorities from a lookup table if required
	 * Note: use $this->priority
	 * @return array id=>priority
	 */
	public function getPriorities()
	{
		return array(
			self::PRIORITY_LOW=>'Low',
			self::PRIORITY_MEDIUM=>'Medium',
			self::PRIORITY_HIGH=>'High',
			self::PRIORITY_URGENT=>'Urgent',
		);
	}
	
	/**
	 * Lookup standard states. Can be later replaced with a databse lookup
	 * @return array, associative array of states
	 */
	public function getStates()
	{
		return array(
			array(
				'id'=>self::STATUS_OPEN, 
				'status'=>'Open', 
				'customer_view'=>'Being Processed'
			),
			array(
				'id'=>self::STATUS_PENDING, 
				'status'=>'Pending', 
				'customer_view'=>'Awaiting your Reply'
			),
			array(
				'id'=>self::STATUS_RESOLVED, 
				'status'=>'Resolved', 
				'customer_view'=>'This ticket has been Resolved'
			),
			array(
				'id'=>self::STATUS_CLOSED, 
				'status'=>'Closed', 
				'customer_view'=>'This ticket has been Closed'
			),
			array(
				'id'=>self::STATUS_WAITING_ON_CUSTOMER, 
				'status'=>'Waiting on Customer', 
				'customer_view'=>'Awaiting your Reply'
			)
		);
	}
	
	/**
	 * return states in a list suitable for use in a dropdown
	 * @return array id => state
	 */
	public function getStatesListData()
	{
		return CHtml::listData(SupportTicket::model()->getStates(), 'id', 'status');
	}
	
	/**
	 * Lookup the type
	 * @return string
	 * @throws CException
	 */
	public function getType()
	{
		$types = $this->getTypes();
		if (!array_key_exists($this->type_id, $types))
			throw new CException("Can not find type with id '{$this->type_id}'");
		return $types[$this->type_id];
	}
	
	/**
	 * gets the status text for this ticket
	 * @return string
	 * @throws CException
	 */
	public function getStatus()
	{
		foreach($this->getStates() as $states)
			if ($states['id'] == $this->status_id)
				return $states['status'];
		throw new CException("Status not found with id '{$this->status_id}'");
	}
	
	/**
	 * Get the priority text
	 * @return string
	 * @throws CException
	 */
	public function getPriority()
	{
		$priorities = $this->getPriorities();
		if (!array_key_exists($this->priority_id, $priorities))
			throw new CException("Can not find priority with id '{$this->priority_id}'");
		return $priorities[$this->priority_id];
	}
	
	/**
	 * get the route to an individual ticket page
	 * @return array
	 */
	public function getRoute()
	{
		return TicketRoute::getRouteTicketItem($this->id);
	}
	
	/**
	 * Get the route to the restore url
	 * @return array route
	 */
	public function getRestoreRoute()
	{
		return TicketRoute::getRouteTicketRestore($this->id);
	}
	
	/**
	 * Get the route to the delete action (trashes the ticket)
	 * @return array route
	 */
	public function getDeleteRoute()
	{
		return TicketRoute::getRouteTicketDelete($this->id);
	}
	
	/**
	 * Per page load cache of next record
	 * @var SupportTicket | null
	 */
	private $_nextRecord = null;
	
	/**
	 * Using the current record ($this model) as a reference get the next model
	 * @return SupportTicket | null
	 */
	public function getNextRecord()
	{
		if ($this->_nextRecord === null) {
			$this->_nextRecord = self::model()->find(array(
				'condition' => 'id > :id',
				'order' => 'id ASC',
				'limit' => 1,
				'params'=>array(':id'=>$this->id),
			));
		}
		return $this->_nextRecord;
	}
	
	/**
	 * Per page load cache of previous record
	 * @var SupportTicket | null
	 */
	private $_previousRecord = null;
	
	/**
	 * Using the current recoed ($this model) as a referecne get the previous model
	 * @return SupportTicket | null
	 */
	public function getPreviousRecord()
	{
		if ($this->_previousRecord===null) {
			$this->_previousRecord = self::model()->find(array(
				'condition' => 'id < :id',
				'order' => 'id DESC',
				'limit' => 1,
				'params'=>array(':id'=>$this->id),
			));
		}
		return $this->_previousRecord;
	}
	
	public function getAgent()
	{
		return 'unknown';
	}
	
	
	public function getSla()
	{
		// find the SLA to apply to this ticket.
	}
	
	public function processSla()
	{
		// set the due date from the SLA
		if ($this->priority_id == self::PRIORITY_LOW) {
			$this->due = self::PRIORITY_LOW;
		}
	}
	
	public function defaultSla()
	{
		return array(
			self::PRIORITY_LOW=>array(
				'respond'=>'1 day',
				'resolve'=>'3 days'
			),
			self::PRIORITY_MEDIUM=>array(
				'respond'=>'8 hours',
				'resolve'=>'1 day'
			),
			self::PRIORITY_HIGH=>array(
				'respond'=>'4 hours',
				'resolve'=>'12 hours'
			),
			self::PRIORITY_URGENT=>array(
				'respond'=>'1 hours',
				'resolve'=>'4 hours'
			)
		);
	}
	
	/**
	 * Get the ticket responses
	 * array of SupportResponse records
	 * @return array SupportResponse
	 */
	public function getResponses()
	{
		$responses = SupportResponse::model()->findAllByAttributes(array('ticket_id'=>$this->id), array('order'=>'created_at DESC'));
		return $responses;
	}
	
}