<?php

/**
 * SupportTicket class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Ticket Model 
 */
class SupportResponse extends NActiveRecord
{
	
	public function tableName()
	{
		return '{{support_response}}';
	}
	
	/**
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('from, message', 'required'),
			array('via, cc', 'safe'),
		);
	}
	
	/**
	 * Ticket table schema
	 * @return array
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'ticket_id'=>'int',
				'email_id'=>'int',
				// store the original from email address
				'from'=>'string',
				'cc'=>'text',
				'subject'=>'text',
				'message'=>'text',
				'agent_id'=>'int not null default 0',
				// string representing where the ticket was raised from
				// i.e. email, phone, web form etc
				'via'=>'string not null default \'system\'',
			)
		);
	}
	
	/**
	 * Apply behaviors
	 * @return array
	 */
	public function behaviors()
	{
		return array(
			'trash'=>array(
				'class'=>'nii.components.behaviors.NTrashBinBehavior',
			),
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
            )
		);
	}
	
	/**
	 * Get static model instance
	 * @param string $className
	 * @return SupportTicket
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}